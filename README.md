# Detecting drift in deep learning: A methodology primer

This repository contains the synthetic dataset used for the paper "Detecting drift in deep learning: A methodology primer". Each image is an automatically generated invoice. The following is the structure of the repository:
1. No Drift: In this folder, it is possible to find the invoices in the same distribution as the one used for training the model.
2. Background Color: Contains invoices with a colored background.
3. Background Image: The invoices have logos on the background.
